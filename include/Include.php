<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/file/library/ConstFile.php');
include($strRootPath . '/src/file/library/ToolBoxFile.php');
include($strRootPath . '/src/file/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/api/FileInterface.php');
include($strRootPath . '/src/file/model/DefaultFile.php');

include($strRootPath . '/src/file/base64/library/ConstBase64File.php');
include($strRootPath . '/src/file/base64/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/base64/model/Base64File.php');

include($strRootPath . '/src/file/name/api/NameFileInterface.php');
include($strRootPath . '/src/file/name/model/DefaultNameFile.php');

include($strRootPath . '/src/file/name/file_system/library/ConstFsFile.php');
include($strRootPath . '/src/file/name/file_system/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/name/file_system/model/FsFile.php');

include($strRootPath . '/src/file/factory/library/ConstFileFactory.php');
include($strRootPath . '/src/file/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/file/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/factory/api/FileFactoryInterface.php');
include($strRootPath . '/src/file/factory/model/DefaultFileFactory.php');

include($strRootPath . '/src/file/factory/standard/library/ConstStandardFileFactory.php');
include($strRootPath . '/src/file/factory/standard/model/StandardFileFactory.php');

include($strRootPath . '/src/file/factory/name/library/ConstNameFileFactory.php');
include($strRootPath . '/src/file/factory/name/model/NameFileFactory.php');

include($strRootPath . '/src/register/directory/library/ConstDirRegister.php');
include($strRootPath . '/src/register/directory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/register/directory/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/register/directory/exception/GetConfigInvalidFormatException.php');
include($strRootPath . '/src/register/directory/exception/SetConfigInvalidFormatException.php');
include($strRootPath . '/src/register/directory/model/DirRegister.php');

include($strRootPath . '/src/view/template/repository/directory/library/ConstDirTmpRepository.php');
include($strRootPath . '/src/view/template/repository/directory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/view/template/repository/directory/exception/FileNotFoundException.php');
include($strRootPath . '/src/view/template/repository/directory/model/DirTmpRepository.php');