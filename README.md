LibertyCode_File
================



Description
-----------

Library contains file components, 
to manage files and use file system features, 
for other libraries components.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/file ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/file": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### File

File allows to get information and content, for specific file.

_Elements_

- File

    Allows to design a file,
    which is an item containing configuration, 
    to get specific file information and content.

- Base64File

    Extends file features. 
    Allows to get file information and content,
    from specified base 64 data source.

- NameFile

    Extends file features. 
    Allows to design a named file,
    to get file name information.
    
- FsFile

    Extends named file features. 
    Allows to get file information and content,
    from file system.
    
- FileFactory

    Allows to design file factory, 
    to provide new or specified file instances,
    from specified configuration.
    
- StandardFileFactory

    Extends file factory features. 
    Provides file instance.
    
- NameFileFactory

    Extends file factory features. 
    Provides named file instance.
    
```php
// Get file factory
use liberty_code\file\file\factory\name\model\NameFileFactory;
$fileFactory = new NameFileFactory();
...
// Get new file from configuration
$file = $fileFactory->getObjFile(array(...));
...
```

#### Register

Register using file system as storage support.

_Elements_

- DirRegister 

    Allows to manage items, 
    using file system as support.
    It use specific directory, 
    where each item stored on specific file.

_Example_

```php
use liberty_code\file\register\directory\model\DirRegister;
$register = new DirRegister();
...
$register->putItem('key_1', '...'); // Register specified item for key 1
$register->putItem('key_N', '...'); // Register specified item for key N
...
foreach($register->getTabKey() as $key) {
    var_dump($register->getItem($key));
}
/**
 * Show: 
 * item for key 1
 * item for key N
 */
...
```

#### Template repository

Template repository used on view, 
using file system as storage support.

_Elements_

- DirTmpRepository

    Extends template repository features.
    It uses list of directory paths, 
    where template files located,
    to load and get specified template content.
    
_Example_

```php
// Get repository
use liberty_code\file\view\template\repository\directory\model\DirTmpRepository;
$repository = new DirTmpRepository([
    'dir_path' => ['... directory path']
]);
...
// Get template content, if required
if($repository->checkExists('template_key'))
{
    var_dump($repository->getStrContent('template_key'));
}
...
```

---


