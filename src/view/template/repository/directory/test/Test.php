<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\file\view\template\repository\directory\model\DirTmpRepository;



// Init directory template repository
$objDirTmpRepo = new DirTmpRepository(
    array(
        'dir_path' => [
            $strRootAppPath . '/src/view/template/repository/directory/test/template/text',
            [
                'dir_path' => $strRootAppPath . '/src/view/template/repository/directory/test/template/html',
                'select_file_extension' => 'html',
                'order' => -1
            ],
            [
                'dir_path' => $strRootAppPath . '/src/view/template/repository/directory/test/template/data',
                'select_file_extension' => ['json', 'xml'],
                'select_key_namespace' => ['data', 'dt']
            ]
        ],
        'key_path_separator' => '.',
        //'select_dir_path_first_require' => true
    )
);



// Test config property
echo('Test directory template repository config: <br />');

try{
    $objDirTmpRepo->setTabConfig(array('dir_path' => 'test'));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test directory template repository config: <pre>');var_dump($objDirTmpRepo->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test template check, get content
$tabKey = array(
    'view.Template1', // Ok: found: html template 1
    'view.Template2', // Ok: found: html template 2
    'Template3', // Ko: not found
    'view.Template3', // Ok: found: text template 3
    'view.Template4', // Ok: found: text template 4
    7, // Ko: bad key format
    'data.view.Template4', // Ok: found: data template 4
    'test.Template5', // Ko: not found: no namespace like test
    'dt.Template5', // Ok: found: data template 5
    'dt.Template6' // Ko: not found: due to file extension
);

foreach($tabKey as $strKey)
{
    echo('Test check template, get template content"'.$strKey.'": <br />');
    try{
        echo('Check exists: <pre>');var_dump($objDirTmpRepo->checkExists($strKey));echo('</pre>');
        echo('Get content: <pre>');var_dump($objDirTmpRepo->getStrContent($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');


