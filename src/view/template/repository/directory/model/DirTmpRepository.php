<?php
/**
 * Description :
 * This class allows to define directory template repository class.
 * Directory template repository is default template repository,
 * using list of directory paths, where template files located,
 * to load and get specified template content,
 * from specified template key, considered as template file path.
 *
 * Directory template repository uses the following specified configuration:
 * [
 *     Default template repository configuration,
 *
 *     dir_path(required): [
 *         // Directory path 1
 *         "string directory path 1, where template files located"
 *
 *         OR
 *
 *         [
 *             dir_path(required): "string directory path 1, where template files located",
 *
 *             select_file_extension(optional: no extension selection done, if not found):
 *                 "string extension,
 *                 used to select template files, which names end with following extension,
 *                 available for specific directory path"
 *                 OR
 *                 ["string extension 1", ..., "string extension N"],
 *
 *             select_key_regexp(optional: no REGEXP selection done, if not found):
 *                 "string REGEXP pattern,
 *                 used to select template keys, which match with following REGEXP pattern,
 *                 available for specific directory path",
 *
 *             select_key_namespace(optional: no namespace selection done, if not found):
 *                 "string namespace,
 *                 used to select template keys, which start with following namespace,
 *                 available for specific directory path"
 *                 OR
 *                 ["string namespace 1", ..., "string namespace N"],
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Directory path N
 *         ...
 *     ],
 *
 *     key_path_separator(optional: got '/', if not found):
 *         "string path separator, used in template keys",
 *
 *     select_dir_path_first_require(optional: got false if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\view\template\repository\directory\model;

use liberty_code\view\template\repository\model\DefaultTmpRepository;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\library\ConstTmpRepository;
use liberty_code\file\view\template\repository\directory\library\ConstDirTmpRepository;
use liberty_code\file\view\template\repository\directory\exception\ConfigInvalidFormatException;
use liberty_code\file\view\template\repository\directory\exception\FileNotFoundException;



class DirTmpRepository extends DefaultTmpRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
     */
    public function __construct(
        array $tabConfig,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Validation
        try
        {
            switch($key)
            {
                case ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check select directory path first option required,
     * used to select directory path, on same order.
     *
     * @return boolean
     */
    public function checkSelectDirPathFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function checkExistsEngine($strKey)
    {
        // Init var
        $strFilePath = $this->getStrFilePath($strKey);
        $result = (!is_null($strFilePath));

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string key path separator.
     *
     * @return string
     */
    protected function getStrKeyPathSeparator()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_KEY_PATH_SEPARATOR]) ?
                $tabConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_KEY_PATH_SEPARATOR] :
                '/'
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of directory path configurations.
     *
     * @param string $strKey = null
     * @param null|boolean $sortAsc = null
     * @return array
     */
    protected function getTabDirPathConfig($strKey = null, $sortAsc = null)
    {
        // Init var
        $strKey = (is_string($strKey) ? $strKey : null);
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();
        $strKeyPathSeparator = $this->getStrKeyPathSeparator();
        $result = array_values($tabConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]);

        // Filter directory path configurations, if required
        if(!is_null($strKey))
        {
            $checkKeySelectedFromNamespace = function($strKey, $namespace) use ($strKeyPathSeparator)
            {
                $result = false;
                $tabNamespace = (is_array($namespace) ? array_values($namespace) : array($namespace));

                // Run each namespace
                for($intCpt = 0; ($intCpt < count($tabNamespace)) && (!$result); $intCpt++)
                {
                    // Check key starts with prefix
                    $strNamespace = $tabNamespace[$intCpt];
                    $result = ToolBoxString::checkStartsWith(
                        $strKey,
                        sprintf('%1$s%2$s', $strNamespace, $strKeyPathSeparator)
                    );
                }

                return $result;
            };

            $result = array_filter(
                $result,
                function($dirPathConfig) use ($checkKeySelectedFromNamespace, $strKey) {
                    return (
                        (!is_array($dirPathConfig)) ||
                        (
                            (
                                (!array_key_exists(ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP, $dirPathConfig)) ||
                                (preg_match($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP], $strKey) == 1)
                            ) &&
                            (
                                (!array_key_exists(ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE, $dirPathConfig)) ||
                                $checkKeySelectedFromNamespace($strKey, $dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE])
                            )
                        )
                    );
                }
            );
        }

        // Sort directory path configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($dirPathConfig1, $dirPathConfig2)
                {
                    $intOrder1 = (
                        (
                            is_array($dirPathConfig1) &&
                            array_key_exists(ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER, $dirPathConfig1)
                        ) ?
                            $dirPathConfig1[ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        (
                            is_array($dirPathConfig2) &&
                            array_key_exists(ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER, $dirPathConfig2)
                        ) ?
                            $dirPathConfig2[ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($tabDirPathConfig);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of string template file paths,
     * from specified template key,
     * and specified directory path configuration.
     *
     * @param string $strKey
     * @param string|array $dirPathConfig
     * @return array
     */
    protected function getTabFilePathFromDirPathConfig($strKey, $dirPathConfig)
    {
        // Init var
        $result = array();
        $strPathSeparator = '/';
        $strKeyPathSeparator = $this->getStrKeyPathSeparator();

        // Get directory path
        $strDirPath = (
            isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) ?
                $dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH] :
                $dirPathConfig
        );

        // Get relative template file path
        $strRelFilePath = $strKey;
        if(isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE]))
        {
            $tabNamespace = $dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE];
            $tabNamespace = (is_array($tabNamespace) ? array_values($tabNamespace) : array($tabNamespace));

            // Run each namespace, and try to found matched namespace
            $strNamespace = null;
            $boolFound = false;
            for($intCpt = 0; ($intCpt < count($tabNamespace)) && (!$boolFound); $intCpt++)
            {
                $strNamespace = sprintf('%1$s%2$s', $tabNamespace[$intCpt], $strKeyPathSeparator);
                $boolFound = ToolBoxString::checkStartsWith($strKey, $strNamespace);
            }

            // Truncate namespace, if required
            if($boolFound)
            {
                $strRelFilePath = substr($strKey, strlen($strNamespace));
            }
        }
        $strRelFilePath = str_replace($strKeyPathSeparator, $strPathSeparator, $strRelFilePath);

        // Get index array of file extensions
        $tabFileExt = (
            isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION]) ?
                $dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION] :
                '*'
        );
        $tabFileExt = (is_array($tabFileExt) ? array_values($tabFileExt) : array($tabFileExt));

        // Run each file extension
        foreach($tabFileExt as $strFileExt)
        {
            $strFilePathPattern = sprintf(
                '%1$s%2$s%3$s.%4$s',
                $strDirPath,
                $strPathSeparator,
                $strRelFilePath,
                $strFileExt
            );

            // Register file paths, if required
            $tabFilePath = glob($strFilePathPattern);
            $result = array_merge(
                $result,
                (
                    is_array($tabFilePath) ?
                        array_filter(
                            array_values($tabFilePath),
                            function($strFilePath) {return (is_file($strFilePath) && is_readable($strFilePath));}
                        ) :
                        array()
                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of string template file paths,
     * from specified template key.
     *
     * @param string $strKey
     * @return array
     */
    protected function getTabFilePath($strKey)
    {
        // Init var
        $result = array();
        $tabDirPathConfig = $this->getTabDirPathConfig($strKey, true);
        $boolSelectFirst = $this->checkSelectDirPathFirstRequired();

        // Run each directory path configuration
        $intOrderRef = null;
        $boolOrderValid = true;
        for(
            $intCpt = 0;
            ($intCpt < count($tabDirPathConfig)) &&
            (
                (count($result) == 0) ||
                (!$boolSelectFirst)
            ) &&
            $boolOrderValid;
            $intCpt++
        )
        {
            // Get info
            $dirPathConfig = $tabDirPathConfig[$intCpt];
            $intOrder =  (
                (
                    is_array($dirPathConfig) &&
                    array_key_exists(ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER, $dirPathConfig)
                ) ?
                    $dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER] :
                    0
            );
            $boolOrderValid = (is_null($intOrderRef) || ($intOrderRef == $intOrder));

            // Get and register array of file paths, if required
            if($boolOrderValid)
            {
                // Get array of file paths
                $tabFilePath = $this->getTabFilePathFromDirPathConfig($strKey, $dirPathConfig);

                // Register array of file paths, if required
                if(count($tabFilePath) > 0)
                {
                    // Init order reference, if required
                    $intOrderRef = (is_null($intOrderRef) ? $intOrder : $intOrderRef);

                    // Register array of file paths
                    $result = $tabFilePath;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string template file path,
     * from specified template key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrFilePath($strKey)
    {
        // Init var
        $tabFilePath = $this->getTabFilePath($strKey);
        $result = (isset($tabFilePath[0]) ? $tabFilePath[0] : null);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws FileNotFoundException
     */
    protected function getStrContentEngine($strKey)
    {
        // Init var
        $strFilePath = $this->getStrFilePath($strKey);

        // Check file found
        if(is_null($strFilePath))
        {
            throw new FileNotFoundException($strKey);
        }

        // Return result
        return file_get_contents($strFilePath);
    }



}