<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\view\template\repository\directory\exception;

use Exception;

use liberty_code\file\view\template\repository\directory\library\ConstDirTmpRepository;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDirTmpRepository::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init directory path configuration array check function
        $checkTabDirPathConfigIsValid = function($tabDirPathConfig) use ($checkTabStrIsValid)
        {
            $result = is_array($tabDirPathConfig) && (count($tabDirPathConfig) > 0);

            // Check each directory path configuration valid, if required
            if($result)
            {
                $tabDirPathConfig = array_values($tabDirPathConfig);
                for($intCpt = 0; ($intCpt < count($tabDirPathConfig)) && $result; $intCpt++)
                {
                    $dirPathConfig = $tabDirPathConfig[$intCpt];
                    $result = (
                        // Check valid directory path
                        (
                            is_string($dirPathConfig) &&
                            is_dir($dirPathConfig) &&
                            is_readable($dirPathConfig)
                        ) ||

                        (
                            is_array($dirPathConfig) &&

                            // Check valid directory path
                            isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&
                            is_string($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&
                            is_dir($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&
                            is_readable($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&

                            // Check valid selection file extension
                            (
                                (!isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION])) ||
                                (
                                    is_string($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION]) &&
                                    (trim($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION]) != '')
                                ) ||
                                (
                                    is_array($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION]) &&
                                    $checkTabStrIsValid($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_FILE_EXTENSION])
                                )
                            ) &&

                            // Check valid selection key REGEXP pattern
                            (
                                (!isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP])) ||
                                (
                                    is_string($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) &&
                                    (trim($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) != '')
                                )
                            ) &&

                            // Check valid selection key namespace
                            (
                                (!isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE])) ||
                                (
                                    is_string($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE]) &&
                                    (trim($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE]) != '')
                                ) ||
                                (
                                    is_array($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE]) &&
                                    $checkTabStrIsValid($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE])
                                )
                            ) &&

                            // Check valid order
                            (
                                (!isset($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER])) ||
                                is_int($dirPathConfig[ConstDirTmpRepository::TAB_CONFIG_KEY_ORDER])
                            )
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid directory path
            isset($config[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&
            $checkTabDirPathConfigIsValid($config[ConstDirTmpRepository::TAB_CONFIG_KEY_DIR_PATH]) &&

            // Check valid key path separator
            (
                (!isset($config[ConstDirTmpRepository::TAB_CONFIG_KEY_KEY_PATH_SEPARATOR])) ||
                (
                    is_string($config[ConstDirTmpRepository::TAB_CONFIG_KEY_KEY_PATH_SEPARATOR]) &&
                    (trim($config[ConstDirTmpRepository::TAB_CONFIG_KEY_KEY_PATH_SEPARATOR]) != '')
                )
            ) &&

            // Check valid select directory path first required option
            (
                (!isset($config[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE]) ||
                    is_int($config[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstDirTmpRepository::TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}