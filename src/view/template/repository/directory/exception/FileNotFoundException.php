<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\view\template\repository\directory\exception;

use Exception;

use liberty_code\file\view\template\repository\directory\library\ConstDirTmpRepository;



class FileNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstDirTmpRepository::EXCEPT_MSG_FILE_NOT_FOUND, strval($strKey));
	}



}