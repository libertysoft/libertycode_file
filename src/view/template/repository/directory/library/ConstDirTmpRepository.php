<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\view\template\repository\directory\library;



class ConstDirTmpRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_DIR_PATH = 'dir_path';
    const TAB_CONFIG_KEY_SELECT_FILE_EXTENSION = 'select_file_extension';
    const TAB_CONFIG_KEY_SELECT_KEY_REGEXP = 'select_key_regexp';
    const TAB_CONFIG_KEY_SELECT_KEY_NAMESPACE = 'select_key_namespace';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_KEY_PATH_SEPARATOR = 'key_path_separator';
    const TAB_CONFIG_KEY_SELECT_DIR_PATH_FIRST_REQUIRE = 'select_dir_path_first_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the directory template repository configuration standard.';
    const EXCEPT_MSG_FILE_NOT_FOUND = 'File not found, for following key "%1$s"!';



}