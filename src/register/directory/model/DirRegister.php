<?php
/**
 * Description :
 * This class allows to define directory register class.
 * Directory register is default register, using file system as support.
 *
 * Items are stored in files like:
 * - File name = key
 * - File body = serialization of following item data array:
 * [
 *     item(required): mixed item,
 *     expire_timeout_datetime(optional): DateTime object
 * ]
 *
 * Directory register uses the following specified configuration:
 * [
 *     store_dir_path(optional: got __DIR__ . '/../../..' if not found):
 *         "string directory path, where items stored",
 *
 *     get_timezone_name(optional: got date_default_timezone_get() if not found):
 *         "string timezone name, used to render item expiration datetime",
 *
 *     set_timezone_name(optional: got 'UTC' if not found):
 *         "string timezone name, used to register item expiration datetime",
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\register\directory\model;

use liberty_code\register\register\model\DefaultRegister;

use DateTime;
use DateTimeZone;
use DateInterval;
use liberty_code\register\item\exception\ItemInvalidFormatException;
use liberty_code\register\register\exception\KeyFoundException;
use liberty_code\register\register\exception\KeyNotFoundException;
use liberty_code\file\register\directory\library\ConstDirRegister;
use liberty_code\file\register\directory\exception\ConfigInvalidFormatException;
use liberty_code\file\register\directory\exception\KeyInvalidFormatException;
use liberty_code\file\register\directory\exception\GetConfigInvalidFormatException;
use liberty_code\file\register\directory\exception\SetConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get config array.
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
class DirRegister extends DefaultRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * Constructor
     *
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDirRegister::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstDirRegister::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstDirRegister::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstDirRegister::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified item data is valid.
     *
     * Item data array format:
     * @see DirRegister item data array format.
     *
     * @param array $itemData
     * @return boolean
     */
    protected function checkItemDataValid(array $itemData)
    {
        // Init var
        $objExpireDt = $this->getObjExpireTimeoutDtFromItemData($itemData);
        $objDtNow = (new DateTime())
            ->setTimezone(
                new DateTimeZone($this->getStrSetTimezoneName())
            );
        $result = (
            // Check item found
            isset($itemData[ConstDirRegister::TAB_ITEM_DATA_KEY_ITEM]) &&

            // Check expiration
            (
                is_null($objExpireDt) ||
                ($objExpireDt > $objDtNow)
            )
        );

        // Return result
        return $result;
    }





	// Methods getters
    // ******************************************************************************

    /**
     * Get directory path,
     * where items stored.
     *
     * @return string
     */
    public function getStrStoreDirPath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH, $tabConfig) ?
                $tabConfig[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH] :
                __DIR__ . '/../../libertycode_file'
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * used to render item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrGetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDirRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstDirRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * used to register item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrSetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDirRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstDirRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME] :
                'UTC'
        );

        // Return result
        return $result;
    }



    /**
     * Get string file path,
     * from specified key.
     *
     * @param string $strKey
     * @return string
     * @throws KeyInvalidFormatException
     */
    protected function getStrFilePathFromKey($strKey)
    {
        // Set check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return $this->getStrStoreDirPath() . '/' . $strKey;
    }



    /**
     * Get string key,
     * from specified file path.
     *
     * @param string $strFilePath
     * @return string
     */
    protected function getStrKeyFromFilePath($strFilePath)
    {
        // Return result
        return basename($strFilePath);
    }



    /**
     * Get item data,
     * from specified item.
     * Overwrite it to implement specific values formatting.
     *
     * Configuration array format:
     * @see addItem() configuration array format.
     *
     * Return array format:
     * @see DirRegister item data array format.
     *
     * @param mixed $item
     * @param array $tabConfig = null
     * @return array
     */
    protected function getItemDataFromItem($item, array $tabConfig = null)
    {
        // Init var
        $result = array(
            ConstDirRegister::TAB_ITEM_DATA_KEY_ITEM => $item,
            ConstDirRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME => $this->getObjExpireTimeoutDtFromConfig($tabConfig)
        );

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified key.
     *
     * Return array format:
     * @see DirRegister item data array format.
     *
     * Remove success return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param string $strKey
     * @param boolean &$boolRemoveSuccess = true
     * @return null|array
     */
    protected function getItemDataFromKey(
        $strKey,
        &$boolRemoveSuccess = true
    )
    {
        // Init var
        $result = null;
        $boolRemoveSuccess = true;
        $strFilePath = $this->getStrFilePathFromKey($strKey);

        // Check file found
        if(is_file($strFilePath))
        {
            // Get item data, if required
            $itemData = $this->getItemDataFromItemDataSet(file_get_contents($strFilePath));
            $result = (
                (is_array($itemData) && $this->checkItemDataValid($itemData)) ?
                    $itemData :
                    $result
            );

            // Remove item, if required
            if(is_null($result))
            {
                $boolRemoveSuccess = (unlink($strFilePath) !== false);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified item data, used in support, to be set.
     * Overwrite it to implement specific feature.
     *
     * Return array format:
     * @see DirRegister item data array format.
     *
     * @param string $strItemData
     * @return array
     */
    protected function getItemDataFromItemDataSet($strItemData)
    {
        // Return result
        return unserialize($strItemData);
    }



    /**
     * Get string item data, used in support, to be set,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see DirRegister item data array format.
     *
     * @param array $itemData
     * @return string
     */
    protected function getStrItemDataSetFromItemData(array $itemData)
    {
        // Return result
        return serialize($itemData);
    }



    /**
     * Get expiration timeout datetime,
     * from specified configuration.
     *
     * Configuration array format:
     * @see addItem() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromConfig(array $tabConfig = null)
    {
        // Init var
        $result = (
            (
                (!is_null($tabConfig)) &&
                array_key_exists(ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME, $tabConfig)
            ) ?
                // Set specific time
                (
                    ($tabConfig[ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] instanceof DateTime) ?
                        $tabConfig[ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] :
                        (new DateTime())->setTimestamp($tabConfig[ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME])
                ) :
                (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND, $tabConfig)
                    ) ?
                        // Add specific second, from datetime now
                        (new DateTime())->add(new DateInterval(sprintf(
                            'PT%1$dS',
                            $tabConfig[ConstDirRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND]
                        ))) :
                        null
                )
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrSetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see DirRegister item data array format.
     *
     * @param array $itemData
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromItemData(array $itemData)
    {
        // Init var
        $result = (
            (
                array_key_exists(ConstDirRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME, $itemData) &&
                ($itemData[ConstDirRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME] instanceof DateTime)
            ) ?
                $itemData[ConstDirRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME] :
                null
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrSetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified key.
     *
     * @param string $strKey
     * @return null|DateTime
     */
    public function getObjExpireTimeoutDt($strKey)
    {
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getObjExpireTimeoutDtFromItemData($itemData)
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrGetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get item,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see DirRegister item data array format.
     *
     * @param array $itemData
     * @return null|mixed
     */
    protected function getItemFromItemData(array $itemData)
    {
        // Init var
        $result = (
            array_key_exists(ConstDirRegister::TAB_ITEM_DATA_KEY_ITEM, $itemData) ?
                $itemData[ConstDirRegister::TAB_ITEM_DATA_KEY_ITEM] :
                null
        );

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getItem($strKey)
	{
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getItemFromItemData($itemData)
        );

        // Return result
        return $result;
	}



    /**
     * Selection configuration format:
     * [
     *     regexp_key(optional): "string REGEXP pattern",
     *
     *     list_key(optional): [
     *         "string key 1",
     *         ...,
     *         "string key N"
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws GetConfigInvalidFormatException
     */
    public function getTabKey(array $tabConfig = null)
    {
        // Check arguments
        GetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = array();
        $tabFilePath = glob($this->getStrStoreDirPath() . '/*');
        $strGetRegexpKey = (
        (
            is_array($tabConfig) &&
            array_key_exists(ConstDirRegister::TAB_GET_CONFIG_KEY_REGEXP_KEY, $tabConfig)
        ) ?
            $tabConfig[ConstDirRegister::TAB_GET_CONFIG_KEY_REGEXP_KEY] :
            null
        );
        $tabGetKey = (
        (
            is_array($tabConfig) &&
            array_key_exists(ConstDirRegister::TAB_GET_CONFIG_KEY_LIST_KEY, $tabConfig)
        ) ?
            $tabConfig[ConstDirRegister::TAB_GET_CONFIG_KEY_LIST_KEY] :
            null
        );

        // Run each file path
        foreach($tabFilePath as $strFilePath)
        {
            // Get key from file, if required
            if(is_file($strFilePath))
            {
                $strKey = $this->getStrKeyFromFilePath($strFilePath);

                // Register key, if required
                if(
                    (
                        (is_null($strGetRegexpKey)) ||
                        (preg_match($strGetRegexpKey, $strKey) === 1)
                    ) &&
                    (
                        (is_null($tabGetKey)) ||
                        (in_array($strKey, $tabGetKey))
                    )
                )
                {
                    $result[] = $strKey;
                }
            }
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods setters
    // ******************************************************************************

    /**
     * Setting Configuration array format:
     * @see addItem() configuration array format.
     *
     * @inheritdoc
     * @throws ItemInvalidFormatException
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Check arguments
        ItemInvalidFormatException::setCheck($item);

        // Return result
        return parent::setItem($strKey, $item, $tabConfig);
    }



	/**
     * Setting configuration array format:
     * [
     *     expire_timeout_time(optional): integer timestamp / DateTime,
     *
     *     expire_timeout_second(optional): integer number of seconds, from datetime now
     * ]
     *
	 * @inheritdoc
	 * @throws KeyFoundException
     * @throws ItemInvalidFormatException
     * @throws SetConfigInvalidFormatException
	 */
	public function addItem($strKey, $item, array $tabConfig = null)
	{
        // Check arguments
        ItemInvalidFormatException::setCheck($item);
        SetConfigInvalidFormatException::setCheck($tabConfig);

        // Add item, if not found
        if(!$this->checkItemExists($strKey))
        {
            // Get info
            $strFilePath = $this->getStrFilePathFromKey($strKey);
            $itemData = $this->getItemDataFromItem($item, $tabConfig);
            $strFileContent = $this->getStrItemDataSetFromItemData($itemData);

            // Add file
            $result = (file_put_contents($strFilePath, $strFileContent) !== false);
        }
        // If item found, throw exception
        else
        {
            throw new KeyFoundException($strKey);
        }

        // Return result
        return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws KeyNotFoundException
	 */
	public function removeItem($strKey)
	{
        // Remove item, if found
        if($this->checkItemExists($strKey))
        {
            // Get info
            $strFilePath = $this->getStrFilePathFromKey($strKey);

            // Remove file
            $result = (unlink($strFilePath) !== false);
        }
        // If item not found, throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
	}



    /**
     * Remove all items not valid.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @return boolean
     */
    public function removeItemAllNotValid()
    {
        // Ini var
        $result = true;
        $tabKey = $this->getTabKey();

        // Run each item
        foreach($tabKey as $strKey)
        {
            // Remove item, if required (try getting valid item data, remove else)
            $boolRemoveSuccess = true;
            $this->getItemDataFromKey($strKey, $boolRemoveSuccess);

            $result = $result && $boolRemoveSuccess;
        }

        // Return result
        return $result;
    }



}