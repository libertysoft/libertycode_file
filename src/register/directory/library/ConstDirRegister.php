<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\register\directory\library;



class ConstDirRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_STORE_DIR_PATH = 'store_dir_path';
    const TAB_CONFIG_KEY_GET_TIMEZONE_NAME = 'get_timezone_name';
    const TAB_CONFIG_KEY_SET_TIMEZONE_NAME = 'set_timezone_name';

    // Configuration get
    const TAB_GET_CONFIG_KEY_REGEXP_KEY = 'regexp_key';
    const TAB_GET_CONFIG_KEY_LIST_KEY = 'list_key';

    // Configuration set
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME = 'expire_timeout_time';
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND = 'expire_timeout_second';

    // Item data configuration
    const TAB_ITEM_DATA_KEY_ITEM = 'item';
    const TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME = 'expire_timeout_datetime';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the directory register configuration standard.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT =
        'Following key "%1$s" invalid! The key must be a string, not empty, without specific characters ("/", "\").';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the directory register get configuration standard.';
    const EXCEPT_MSG_SET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the directory register set configuration standard.';



}