<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\register\directory\exception;

use Exception;

use liberty_code\file\register\directory\library\ConstDirRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDirRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabTzNm = timezone_identifiers_list();
        $result =
            // Check valid store directory path
            isset($config[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH]) &&
            is_string($config[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH]) &&
            is_dir($config[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH]) &&
            is_readable($config[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH]) &&
            is_writable($config[ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH]) &&

            // Check valid get timezone name
            (
                (!isset($config[ConstDirRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDirRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstDirRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME], $tabTzNm))
                )
            ) &&

            // Check valid set timezone name
            (
                (!isset($config[ConstDirRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDirRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstDirRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME], $tabTzNm))
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}