<?php
/**
 * Description :
 * This class allows to describe behavior of file factory class.
 * File factory allows to provide new or specified file instances,
 * hydrated with a specified configuration,
 * from a set of potential predefined file types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\factory\api;

use liberty_code\file\file\api\FileInterface;



interface FileFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of file,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrFileClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance file,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param FileInterface $objFile = null
     * @return null|FileInterface
     */
    public function getObjFile(
        array $tabConfig,
        $strConfigKey = null,
        FileInterface $objFile = null
    );
}