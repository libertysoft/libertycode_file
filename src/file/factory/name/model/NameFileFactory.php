<?php
/**
 * Description :
 * This class allows to define named file factory class.
 * Named file factory allows to provide and hydrate named file instance.
 *
 * Named file factory uses the following specified configuration, to get and hydrate named file:
 * [
 *     type(required): "name_file_system",
 *     File system file configuration (@see FsFile configuration)
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\factory\name\model;

use liberty_code\file\file\factory\model\DefaultFileFactory;

use liberty_code\file\file\name\file_system\model\FsFile;
use liberty_code\file\file\factory\name\library\ConstNameFileFactory;



class NameFileFactory extends DefaultFileFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of file, from type
        switch($strConfigType)
        {
            case ConstNameFileFactory::CONFIG_TYPE_NAME_FILE_SYSTEM:
                $result = FsFile::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileNew($strConfigType)
    {
        // Init var
        $result = parent::getObjFileNew($strConfigType);

        // Get class path of file, from type, if required
        if(is_null($result))
        {
            switch($strConfigType)
            {
                case ConstNameFileFactory::CONFIG_TYPE_NAME_FILE_SYSTEM:
                    $result = new FsFile();
                    break;
            }
        }

        // Return result
        return $result;
    }



}