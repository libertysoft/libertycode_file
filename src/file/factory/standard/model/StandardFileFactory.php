<?php
/**
 * Description :
 * This class allows to define standard file factory class.
 * Standard file factory allows to provide and hydrate file instance.
 *
 * Standard file factory uses the following specified configuration, to get and hydrate file:
 * [
 *     type(required): "base64",
 *     Base 64 file configuration (@see Base64File configuration)
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\factory\standard\model;

use liberty_code\file\file\factory\model\DefaultFileFactory;

use liberty_code\file\file\base64\model\Base64File;
use liberty_code\file\file\factory\standard\library\ConstStandardFileFactory;



class StandardFileFactory extends DefaultFileFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of file, from type
        switch($strConfigType)
        {
            case ConstStandardFileFactory::CONFIG_TYPE_BASE64:
                $result = Base64File::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileNew($strConfigType)
    {
        // Init var
        $result = parent::getObjFileNew($strConfigType);

        // Get class path of file, from type, if required
        if(is_null($result))
        {
            switch($strConfigType)
            {
                case ConstStandardFileFactory::CONFIG_TYPE_BASE64:
                    $result = new Base64File();
                    break;
            }
        }

        // Return result
        return $result;
    }



}