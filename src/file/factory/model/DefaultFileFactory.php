<?php
/**
 * Description :
 * This class allows to define default file factory class.
 * Can be consider is base of all file factory types.
 *
 * Default file factory uses the following specified configuration, to get and hydrate file:
 * [
 *     type(optional): "string constant to determine file type",
 *
 *     ... specific file configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\file\file\factory\api\FileFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\factory\library\ConstFileFactory;
use liberty_code\file\file\factory\exception\FactoryInvalidFormatException;
use liberty_code\file\file\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|FileFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|FileFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultFileFactory extends DefaultFactory implements FileFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param FileFactoryInterface $objFactory = null
     */
    public function __construct(
        FileFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init file factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFileFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstFileFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified file.
     * Overwrite it to set specific call hydration.
     *
     * @param FileInterface $objFile
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateFile(FileInterface $objFile, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstFileFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstFileFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate file
        $objFile->setConfig($tabConfigFormat);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFileFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFileFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified file object.
     *
     * @param FileInterface $objFile
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(FileInterface $objFile, array $tabConfigFormat)
    {
        // Init var
        $strFileClassPath = $this->getStrFileClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strFileClassPath)) &&
            ($strFileClassPath == get_class($objFile))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstFileFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstFileFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of file,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrFileClassPathFromType($strConfigType);



    /**
     * Get string class path of file engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrFileClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrFileClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrFileClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrFileClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrFileClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance file,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|FileInterface
     */
    protected function getObjFileNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrFileClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance file engine.
     *
     * @param array $tabConfigFormat
     * @param FileInterface $objFile = null
     * @return null|FileInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjFileEngine(
        array $tabConfigFormat,
        FileInterface $objFile = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objFile = (
            is_null($objFile) ?
                $this->getObjFileNew($strConfigType) :
                $objFile
        );

        // Get and hydrate file, if required
        if(
            (!is_null($objFile)) &&
            $this->checkConfigIsValid($objFile, $tabConfigFormat)
        )
        {
            $this->hydrateFile($objFile, $tabConfigFormat);
            $result = $objFile;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjFile(
        array $tabConfig,
        $strConfigKey = null,
        FileInterface $objFile = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjFileEngine($tabConfigFormat, $objFile);

        // Get file from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjFile($tabConfig, $strConfigKey, $objFile);
        }

        // Return result
        return $result;
    }



}