<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\library;



class ConstFile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_EXTENSION = 'extension';
    const TAB_CONFIG_KEY_MIME_TYPE = 'mime_type';
    const TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE = 'content_mime_type_require';
    const TAB_CONFIG_KEY_SIZE = 'size';
    const TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE = 'content_size_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default file configuration standard.';
}