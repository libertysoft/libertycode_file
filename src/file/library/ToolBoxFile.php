<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxFile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string mime type,
     * from specified file content.
     *
     * @param string $strFileContent
     * @return null|string
     */
    public static function getStrMimeTypeFromFileContent($strFileContent)
    {
        // Init var
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $result = (
            (
                is_string($strFileContent) &&
                (trim($strMimeType = finfo_buffer($fileInfo, $strFileContent)) != '')
            ) ?
                $strMimeType :
                null
        );
        finfo_close($fileInfo);

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from specified file content.
     *
     * @param string $strFileContent
     * @return null|integer
     */
    public static function getIntSizeFromFileContent($strFileContent)
    {
        // Init var
        $result = (
            is_string($strFileContent) ?
                mb_strlen($strFileContent, '8bit') :
                null
        );

        // Return result
        return $result;
    }



}