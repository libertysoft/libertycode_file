<?php
/**
 * This class allows to define base 64 file class.
 * Base 64 file is file, allows to get file information and content,
 * from specified base 64 data source.
 *
 * Base 64 file uses the following specified configuration:
 * [
 *     Default file configuration,
 *
 *     data_source(required): "string base 64 data source"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\base64\model;

use liberty_code\file\file\model\DefaultFile;

use liberty_code\file\file\library\ConstFile;
use liberty_code\file\file\base64\library\ConstBase64File;
use liberty_code\file\file\base64\exception\ConfigInvalidFormatException;



class Base64File extends DefaultFile
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string data source,
     * from configuration.
     *
     * @return string
     */
    public function getStrDataSrc()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstBase64File::TAB_CONFIG_KEY_DATA_SOURCE];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrContent()
    {
        // Init var
        $result = base64_decode($this->getStrDataSrc());

        // Check valid data source
        if(!is_string($result))
        {
            throw new ConfigInvalidFormatException($this->getTabConfig());
        }

        // Return result
        return $result;
    }



}