<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\exception;

use Exception;

use liberty_code\file\file\library\ConstFile;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFile::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid extension
            (
                (!isset($config[ConstFile::TAB_CONFIG_KEY_EXTENSION])) ||
                (
                    is_string($config[ConstFile::TAB_CONFIG_KEY_EXTENSION]) &&
                    (trim($config[ConstFile::TAB_CONFIG_KEY_EXTENSION]) != '')
                )
            ) &&

            // Check valid mime type
            (
                (!isset($config[ConstFile::TAB_CONFIG_KEY_MIME_TYPE])) ||
                (
                    is_string($config[ConstFile::TAB_CONFIG_KEY_MIME_TYPE]) &&
                    (trim($config[ConstFile::TAB_CONFIG_KEY_MIME_TYPE]) != '')
                )
            ) &&

            // Check valid content mime type required option
            (
                (!isset($config[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE]) ||
                    is_int($config[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE]) ||
                    (
                        is_string($config[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE]) &&
                        ctype_digit($config[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE])
                    )
                )
            ) &&

            // Check valid size
            (
                (!isset($config[ConstFile::TAB_CONFIG_KEY_SIZE])) ||
                (
                    is_int($config[ConstFile::TAB_CONFIG_KEY_SIZE]) &&
                    ($config[ConstFile::TAB_CONFIG_KEY_SIZE] >= 0)
                )
            ) &&

            // Check valid content size required option
            (
                (!isset($config[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE]) ||
                    is_int($config[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE]) ||
                    (
                        is_string($config[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE]) &&
                        ctype_digit($config[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}