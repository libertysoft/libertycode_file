<?php
/**
 * Description :
 * This class allows to describe behavior of file class.
 * File is item, containing all information to get file information and content.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\api;



interface FileInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string extension, if possible.
     *
     * @return null|string
     */
    public function getStrExtension();



    /**
     * Get string mime type, if possible.
     *
     * @return null|string
     */
    public function getStrMimeType();



    /**
     * Get string content.
     *
     * @return string
     */
    public function getStrContent();



    /**
     * Get integer size (in bytes).
     *
     * @return integer
     */
    public function getIntSize();





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}