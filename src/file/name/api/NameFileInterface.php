<?php
/**
 * Description :
 * This class allows to describe behavior of named file class.
 * Named file is file, containing all information to get file name.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\name\api;

use liberty_code\file\file\api\FileInterface;



interface NameFileInterface extends FileInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string base name.
     *
     * @return string
     */
    public function getStrBaseName();



    /**
     * Get string name.
     *
     * @return string
     */
    public function getStrName();
}