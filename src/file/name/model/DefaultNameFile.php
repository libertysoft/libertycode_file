<?php
/**
 * This class allows to define default named file class.
 * Can be consider is base of all named file types.
 *
 * Default named file uses the following specified configuration:
 * [
 *     Default file configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\name\model;

use liberty_code\file\file\model\DefaultFile;
use liberty_code\file\file\name\api\NameFileInterface;



abstract class DefaultNameFile extends DefaultFile implements NameFileInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrBaseName()
    {
        // Return result
        return pathinfo($this->getStrName(), PATHINFO_FILENAME);
    }



    /**
     * @inheritdoc
     */
    public function getStrExtension()
    {
        // Init var
        $result = (
            is_null($strExt = $this->getStrExtensionFromConfig()) ?
                // Get extension from name, if required
                (
                    (trim($strExt = pathinfo($this->getStrName(), PATHINFO_EXTENSION)) != '') ?
                        $strExt :
                        null
                ):
                $strExt
        );

        // Return result
        return $result;
    }



}