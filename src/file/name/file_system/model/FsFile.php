<?php
/**
 * This class allows to define file system file class.
 * File system file is file, allows to get file information and content,
 * from file system.
 *
 * File system file uses the following specified configuration:
 * [
 *     Default named file configuration,
 *
 *     full_path(required): "string file full path"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\name\file_system\model;

use liberty_code\file\file\name\model\DefaultNameFile;

use liberty_code\file\file\library\ConstFile;
use liberty_code\file\file\name\file_system\library\ConstFsFile;
use liberty_code\file\file\name\file_system\exception\ConfigInvalidFormatException;



class FsFile extends DefaultNameFile
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        //$result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string full path,
     * from configuration.
     *
     * @return string
     */
    public function getStrFullPath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstFsFile::TAB_CONFIG_KEY_FULL_PATH];

        // Return result
        return $result;
    }



    /**
     * Get string directory path,
     * from configuration.
     *
     * @return string
     */
    public function getStrDirPath()
    {
        // Return result
        return pathinfo($this->getStrFullPath(), PATHINFO_DIRNAME);
    }



    /**
     * @inheritdoc
     */
    public function getStrName()
    {
        // Return result
        return pathinfo($this->getStrFullPath(), PATHINFO_BASENAME);
    }



    /**
     * @inheritdoc
     */
    public function getStrContent()
    {
        // Return result
        return file_get_contents($this->getStrFullPath());
    }



}