<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Use
use liberty_code\file\file\factory\standard\model\StandardFileFactory;
use liberty_code\file\file\factory\name\model\NameFileFactory;



// Init factory
$objFileFactory = new StandardFileFactory();
$objFileFactory = new NameFileFactory($objFileFactory);


