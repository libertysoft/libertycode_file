<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/file/test/FileTest.php');

// Use
use liberty_code\file\file\name\api\NameFileInterface;



// Init var
$strFilePath1 = 'C:\Users\Sergio\Documents\work\emploi\canada\candidature\cv\CV serge bouzid ca_qc_upd4_en.doc';
$strFilePath2 = 'C:\Users\Sergio\Documents\work\emploi\canada\candidature\cv\CV serge bouzid ca_qc_upd4_en.pdf';



// Test file
$tabTabConfig = array(
    'file_1' => [
        'type' => 'base64',
        'data_source' => 7
    ], // Ko, bad base 64 config format
    'file_2' => [
        'type' => 'base64',
        //'mime_type' => 'test',
        //'content_mime_type_require' => false,
        //'size' => 5,
        //'content_size_require' => false,
        'data_source' => base64_encode(file_get_contents($strFilePath1))
    ], // Ok
    'file_3' => [
        'type' => 7,
        'full_path' => 'test/test.test'
    ], // Ko, bad type format
    'file_4' => [
        'type' => 'name_file_system',
        'full_path' => 'test/test.test'
    ], // Ko, bad file system config format
    'file_5' => [
        'type' => 'name_file_system',
        'full_path' => $strFilePath2
    ], // Ok
    'file_6' => [
        'type' => 'test',
        'full_path' => $strFilePath2
    ], // Ko, type not found
);

foreach($tabTabConfig as $strConfigKey => $tabConfig)
{
    echo('Test file "'.$strConfigKey.'": <br />');
    try{
        $objFile = $objFileFactory->getObjFile($tabConfig, $strConfigKey);

        if(!is_null($objFile))
        {
            echo('Get config: <pre>');var_dump($objFile->getTabConfig());echo('</pre>');
            echo('Get extension: <pre>');var_dump($objFile->getStrExtension());echo('</pre>');
            echo('Get mime type: <pre>');var_dump($objFile->getStrMimeType());echo('</pre>');
            echo('Get size: <pre>');var_dump($objFile->getIntSize());echo('</pre>');
            echo('Get size (Ko): <pre>');var_dump($objFile->getIntSize() / 1024);echo('</pre>');
            //echo('Get content: <pre>');var_dump($objFile->getStrContent());echo('</pre>');

            if($objFile instanceof NameFileInterface)
            {
                echo('Get base name: <pre>');var_dump($objFile->getStrBaseName());echo('</pre>');
                echo('Get name: <pre>');var_dump($objFile->getStrName());echo('</pre>');
            }
        }
        else
        {
            echo('File not provided.');
            echo('<br />');
        }

    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Set render
?>


