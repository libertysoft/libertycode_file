<?php
/**
 * This class allows to define default file class.
 * Can be consider is base of all file types.
 *
 * File uses the following specified configuration:
 * [
 *     extension(optional): "string extension",
 *
 *     mime_type(optional): "string mime type",
 *
 *     content_mime_type_require(optional: got true, if not found):
 *         true / false (If true, try to get mime type from content, if not found),
 *
 *     size(optional): integer size,
 *
 *     content_size_require(optional: got true, if not found):
 *         true / false (If true, try to get size from content, if not found)
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\file\file\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\file\file\api\FileInterface;

use liberty_code\file\file\library\ConstFile;
use liberty_code\file\file\library\ToolBoxFile;
use liberty_code\file\file\exception\ConfigInvalidFormatException;



abstract class DefaultFile extends FixBean implements FileInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFile::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstFile::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFile::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFile::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check content mime type required.
     *
     * @return boolean
     */
    public function checkContentMimeTypeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE])) ||
            (intval($tabConfig[ConstFile::TAB_CONFIG_KEY_CONTENT_MIME_TYPE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check content size required.
     *
     * @return boolean
     */
    public function checkContentSizeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE])) ||
            (intval($tabConfig[ConstFile::TAB_CONFIG_KEY_CONTENT_SIZE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstFile::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get string extension,
     * from configuration, if found.
     *
     * @return null|string
     */
    protected function getStrExtensionFromConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstFile::TAB_CONFIG_KEY_EXTENSION, $tabConfig) ?
                $tabConfig[ConstFile::TAB_CONFIG_KEY_EXTENSION] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrExtension()
    {
        // Return result
        return $this->getStrExtensionFromConfig();
    }



    /**
     * Get string mime type,
     * from configuration, if found.
     *
     * @return null|string
     */
    protected function getStrMimeTypeFromConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstFile::TAB_CONFIG_KEY_MIME_TYPE, $tabConfig) ?
                $tabConfig[ConstFile::TAB_CONFIG_KEY_MIME_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string mime type,
     * from content.
     *
     * @return null|string
     */
    protected function getStrMimeTypeFromContent()
    {
        // Return result
        return ToolBoxFile::getStrMimeTypeFromFileContent($this->getStrContent());
    }



    /**
     * @inheritdoc
     */
    public function getStrMimeType()
    {
        // Init var
        $result = (
            (!is_null($strMimeType = $this->getStrMimeTypeFromConfig())) ?
                // Get mime type from config, if required
                $strMimeType :
                (
                    (
                        $this->checkContentMimeTypeRequired() &&
                        (!is_null($strMimeType = ToolBoxFile::getStrMimeTypeFromFileContent($this->getStrContent())))
                    ) ?
                        // Get mime type from content, if required
                        $strMimeType :
                        null
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get integer size.
     *
     * @return null|integer
     */
    protected function getIntSizeFromConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstFile::TAB_CONFIG_KEY_SIZE, $tabConfig) ?
                $tabConfig[ConstFile::TAB_CONFIG_KEY_SIZE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get integer size,
     * from content.
     *
     * @return null|integer
     */
    protected function getIntSizeFromDataSrc()
    {
        // Return result
        return ToolBoxFile::getIntSizeFromFileContent($this->getStrContent());
    }



    /**
     * @inheritdoc
     */
    public function getIntSize()
    {
        // Init var
        $result = (
            (!is_null($intSize = $this->getIntSizeFromConfig())) ?
                // Get size from config, if required
                $intSize :
                (
                    (
                        $this->checkContentSizeRequired() &&
                        (!is_null($intSize = ToolBoxFile::getIntSizeFromFileContent($this->getStrContent())))
                    ) ?
                        // Get size from content, if required
                        $intSize :
                        null
                )
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstFile::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}